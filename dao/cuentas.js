/**
* @fileoverview Libreria con funciones de acceso a datos de cuentas
*
* @author Miguel Angel Fernandez Valderrama
* @version 1.1
*/

'use strict'
//variable del esquema
var InfoCuenta = require('../models/cuenta')



exports.getAllCuentas = function(req,res){
    console.log("Entrando en la función getAllCuentas.")
		InfoCuenta.find(function(err, listacuentas) {
			if (err){
          console.log(res.statusCode)
			    res.send(err);
      }else{
        console.log(listacuentas)
			     res.json(listacuentas)
      }
		})
}

exports.getInfoCuenta = function(req, res) {
  console.log("Entrando en la función getInfoCuenta.")
  let iban = req.params.IBAN
  //la función findOne recibe en su primer parametro un objeto con este formato {"<nombreCampo>":<valorCampo>}
  InfoCuenta.findOne({"IBAN":iban},'movimientos', (err, infocuenta) => {
    if (err) {
      console.log(err)
      return res.status(500).send({message: 'Error making request: $(err)'})
    }

    if (!infocuenta){
      console.log(infocuenta)
      return res.status(404).send({message: 'La cuenta no existe'})
    }

    res.status(200).send({infoCuenta: infocuenta})

    //console.log(infocuenta) /*La consola no esta arrojando nada*/
  })
} //fin del get(/cuentas/iban/:IBAN)


/**
 * getCuentasCliente
 * Retorna las cuentas de un cliente, se le busca por idcliente
 * 
 * @param {*} req recibe el idcliente
 * @param {*} res retorna la información de la cuenta
 * 
 */

exports.getCuentasCliente = function(req, res){
  console.log("Entrando funcion getCuentasCliente")
  var id = req.headers.idcliente
  InfoCuenta.find({"idcliente":id}, function (err, infocuenta) {
    if (err) {
      return res.status(500).send({message: 'Error making request: $(err)'})
    }

    if (!infocuenta){
      console.log(infocuenta)
      return res.status(404).send({message: 'Usuario no tiene cuentas'})
    }
    console.log(infocuenta)
    res.status(200).send({cuentas: infocuenta})
  })
}//fin de getCuentasCliente get(/cuentas/cliente)

/**
 * createCuenta
 * Crear cuenta, primero busca si la cuenta ya existe, para no crearla
 * 
 * 
 * @param {*} req en el body recibe los diversos datos de una cuenta
 * @param {*} res 
 * 
 */
exports.createCuenta = function(req,res){
    console.log("Entrando en la funcion createCuenta.")
    var body = req.body;
    InfoCuenta.findOne({ IBAN: body.IBAN }, function (err,cuenta) {
      if (err) {
        console.log(err);
      } else {
        if (cuenta) {
          res.status(409);
          res.json({
            "status": 409,
            "message": "Cuenta ya existe."
          });
        } else {
          //creamos nueva instancia
          var newCuenta = new InfoCuenta({
            id: body.id,
            IBAN: body.IBAN,
            idcliente: body.idcliente,
            saldo: body.saldo,
            movimientos: new Array()
          });
          newCuenta.save(function(errCreate,newCuenta) {
            if (errCreate) {
              return console.error(errCreate);
            } else {
              console.log('nuevacuenta')
              console.log(newCuenta)
              res.json(newCuenta);
            }
          });
        }
      }
    })
  }


/**
 * @name getMovimientosCuenta
 * Obtiene los movimientos de una cuenta pasada como parametro en la request
 * 
 * 
 * @param {*} req recibe el id de la cuenta
 * @param {*} res retorna un array con los movimientos de la cuenta seleccionada
 */

exports.getMovimientosCuenta = function(req, res){
  console.log("Ejecutando getMovimientosCuenta");
  console.log(req.headers);
  var idcuenta = parseInt(req.headers.id);
  InfoCuenta.findOne({id: idcuenta}, function (err, movimientos){
    if (err) {
      console.log("getMovimientosCuenta:"+err);
      return res.status(500).send({ message: 'Error haciendo la solicitud.' })
    } else {
      if (!movimientos) {
        console.log('getMovimientosCuenta: cuenta no existe')
        return res.status(404).send({ message: 'La cuenta no existe.' })
      }
      else {
        console.log(movimientos)
        res.status(200).send({ resultado: movimientos })
      }
    }
  })
}

exports.addMovimientos = function(req, res) {
  console.log("Entrando en la funcion addMovimientos.")
  //obtiene todos los parametros que necesita recuperar
  //var idcliente = req.params.idcliente
  var idcuenta = req.params.IBAN
  console.log(req.params);
  var body = req.body;
  console.log(req.body);
  var importe = body.importe;
  //recupera información de la cuenta y sus movimientos
  InfoCuenta.findOne({ IBAN: idcuenta }, function (err,cuenta) {
    if (err) {
      console.log(err);
      return res.status(500).send({message: 'Error haciendo la solicitud.'})
    } else {
      if (!cuenta) {
        console.log('cuenta no existe')
        return res.status(404).send({message: 'La cuenta no existe.'})
      }
      else{
        //la cuenta existe
        console.log('La cuenta existe.');
        console.log(cuenta);
        //verificar si tiene suficiente saldo para registrar el movimiento
        if(cuenta.saldo >= importe){
          console.log('Cuenta tiene saldo suficiente, procedemos a registrar movimiento')
          //actualizamos saldo origen
          cuenta.saldo = cuenta.saldo - importe;
          //agregamos movimiento
          var listaMov = cuenta.movimientos.push({id: cuenta.movimientos.length + 1,
          fecha: body.fecha,
          importe: body.importe*-1,
          moneda: body.moneda,
          fechavalor: body.fechavalor});
          //grabamos cuenta
          cuenta.save(function(errSave, cuenta){
            if (errSave) {
              console.log('error al grabar cuenta actualizada')
              return console.error(errSave);
            } else {
              console.log('Grabamos movimiento')
              res.json(cuenta);
            }
          })
        }
        else{
          console.log('No tiene suficiente saldo')
          return res.status(400).send({message: 'La cuenta no tiene suficiente saldo para efectuar el movimiento'})
        }
      }
    }
  })
}
