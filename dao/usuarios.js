var config = require('../config/config')
var requestJson = require('request-json') // Cliente web

/** Datos base de de acceso a MLAB. Deuda **/
var urlmLabRaiz = config.urlMLabRaiz
var apiKey= config.apiKey

/*
recupera toda la lista de usuarios disponibles en la base de datos
*/

exports.getAllUsuarios = function(req, res){
    var clienteMLab = null
    //recuperar los datos desde base de datos
    clienteMLab = requestJson.createClient(urlmLabRaiz + "/usuarios?" + apiKey)
    //si queremos recuperar los datos desde un fichero en formato json
    return new Promise(function(fulfill, reject)
    {
      clienteMLab.get('', function(errGet, resGet, bodyGet){
        if(!errGet && resGet.statusCode == 200){
          res.send(bodyGet)
          console.log("bodyGet:" + bodyGet)
          fulfill(bodyGet);
        }
        else{
          //res.send("error")
          reject({"code":"USU.001", "text":"Error al recuperar usuarios"})
        }
      })
    })
};
