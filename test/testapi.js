//referencia al framework para pruebas en javascript que se ejecutan sobre Node.js
var mocha = require('mocha')
//libreria de asserciones
var chai = require('chai')

var chaiHttp = require('chai-http')
var server = require('../server')

var should = chai.should()

//configurar chai con modulo http
chai.use(chaiHttp)

//definir una suite de pruebas
describe('Test de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.es')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  })
})


describe('Test Cuentas', () => {
  it('lista todas las cuentas', (done =>{
    chai.request('http://localhost:3000')
      .get('/apitechu/v1/cuentas')
      .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('array')
          done()
      })
  }))
})
/*
 describe('Test de API usuarios', () => {
  it('Raiz OK', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          //ahora probaremos que dentro del mensaje viene un valor determinado
          res.body.mensaje.should.be.eql("Bienvenido a mi api")
          done()
        })
  })
  it("Lista de usuarios", (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          //controla que el tipo que devuelve nuestra api es un array
          res.body.should.be.a("array")
          //valida que cada registro tenga la propiedad de email y password
          for (var i = 0; i < res.body.length; i++) {
            res.body[i].should.have.property("email")
            res.body[i].should.have.property("password")
          }
          done()
        })
  })

})*/
