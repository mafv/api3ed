/*
Filename: borradorproyecto/models/usuario.js

*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  //_id:String,
  id: Number,
  nombre:String,
  apellido:String,
  email:String,
  password: String
});

module.exports = mongoose.model('Usuario', userSchema);

/* Ejemplo de un registro de la colección Usuarios
{
        "_id": "5a85a153cc8b0f42c631a003",
        "id": 1,
        "nombre": "Catlin",
        "apellido": "Bolley",
        "email": "miguel@miguel.com",
        "password": "abcd",
        "logged": true
    },
*/
