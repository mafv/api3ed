var mongoose = require('mongoose');
//definimos Schema del subdocs para los movimientos
var MovimientoSchema =  mongoose.Schema({
    id:Number,
    fecha:String,
    importe:Number,
    moneda:String,
    tipoimporte:String,
    cuentadestino:String,
    fechavalor:String,
    tipooperacion:String
});

//definimos Schema de Cuentas
var cuentaSchema = mongoose.Schema({
  id: Number,
  IBAN:String,
  aliascuenta:String,
  idcliente:String,
  movimientos: [MovimientoSchema],
  saldo:Number,
  moneda:String
});

module.exports = mongoose.model('Cuenta', cuentaSchema);
