/** Declaramos valores de configuración **/

// define config object
var config = {};

//mongolab connection
config.cadenaConexionBD='mongodb://admin:TechU2018@ds235788.mlab.com:35788/bdbancomafv'

config.urlmLabRaiz = "https://api.mlab.com/api/1/databases/bdbancomafv/collections";
//recuperar la key desde mlab en user /api Key
config.apiKey = "apiKey=9yx8GqsgGsghb5iLMbYtSL1TgxJMz3T6";

// JWT salt/secret
config.jwtsecret = "xUspTR6giWEJ96LkDi9SBhChpB8PJUQl4wLOk6MJtmYJSb4XHdNUkNrXWFA9J81";
//tamaño del Salt round
config.BCRYPT_SALT_ROUNDS = 10;
config.DIAS_EXPIRACION = 3;

module.exports = config;
