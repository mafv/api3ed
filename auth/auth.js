var jwt = require('jwt-simple');
var bcrypt = require('bcrypt');
var User = require('../models/usuario');
var config = require('../config/config');

var auth = {

  login: function(req, res) {
    console.log("Ejecutando login")
    var username = req.headers.email || '';
    var password = req.headers.password || '';
    console.log('u: ' + username);
    console.log('p: ' + password);
    if (username == '' || password == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "1. Credenciales inválidos."
      });
      return;
  }

  // Fire a query to your DB and get the user object if it exists
  auth.getUser(username, function(dbUserObj,err) {
    if (!dbUserObj) { // If authentication fails, we send a 401 back
      console.log('Autenticación fallida.')
      res.status(401);
      res.json({
        "status": 401,
        "message": "Credenciales inválidos"
      });
      return;
    }

    if (dbUserObj) {
      console.log('Objeto usuario existe')
      console.log(dbUserObj)
      // If authentication is success, we will generate a token
      // and dispatch it to the client
      bcrypt.compare(password, dbUserObj.password, function(err, passmatch) {

          if (passmatch == true) {
            res.json(genToken(dbUserObj));
          } else {
            res.status(401);
            res.json({
              "status": 401,
              "message": "Credenciales inválidos"
            });
            return;
          }
      });
    }
  });

},

getUser: function(username,callback) {
    User.findOne({ email: username }, function (err,user) {
      if (err) {
        console.log(err);
        callback(false);
      } else {
        console.log("function:getUser:")
        console.log(user)
        callback(user);
      }
    });
},

isUserAdmin: function(username,callback) {
    User.findOne({ email: username }, function (err,user) {
      if (user.role == 'admin') {
        callback(true);
      } else {
        callback(false);
      }
    });
},

encryptPass: function(password,callback) {
  console.log("password recibida:" + password)
    // this will auto-gen a salt
    // bcrypt.hash(password,size of hash, function)
    bcrypt.hash(password, 10, function(err, hash) {
      if (hash) {
        console.log("entramos porque tiene hash")
        callback(hash);
      } else {
        console.log("hay error")
        console.log(err);
      }
    });
}

}

/** definición de métodos privados **/

// generar token
function genToken(user) {
  console.log(user);
  var expires = expiresIn(config.DIAS_EXPIRACION);
  var token = jwt.encode({
    exp: expires
  }, config.jwtsecret);

  return {
    token:    token,
    expires:  expires,
    user:     user.email,
    idcliente:user.id,
    name:     user.nombre,
    apellido: user.apellido
  };

}

function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;
