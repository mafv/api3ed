//por defecto require busca en la carpeta ./modenode
var usuarios = require('./usuarios.json')
var listausuarios = require("./datospractica.json")

//console.log("Hola mundo");

app.get('/apitechu/v1', function(req, res){
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi api"})
})

app.get('/apitechu/v1/usuarios', function(req, res){
  //si queremos recuperar los datos desde un fichero en formato json
  //res.sendfile('usuarios.json')
  res.send(listausuarios)
})

app.post('/apitechu/v1/usuarios', function(req, res){
  //definimos variable nuevo que recoge los valores desde la cabecera
  var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
  //añadimos a la colección
  usuarios.push(nuevo)
  //la petición con los datos llega por cabeceras
  console.log(req.headers)
  //los objetos json son java script y hay que convertirlos en string, para eso se usa la funcion stringify
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios.json", datos, "utf8", function(err){
    if (err)
      console.log(err)
    console.log("Fichero guardado")
    })
  res.send("Alta OK")
})

//lo que hace es identificar que id es un parametro
app.delete('/apitechu/v1/usuarios/:elegido', function(req, res){
  usuarios.splice(req.params.elegido-1, 1)
  res.send("Usuario eliminado")
})

//recurso que recibe dos parametros p1 y p2
//para invocarlo llamamos
app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res){
  console.log("parametros")
  console.log(req.params)
  console.log("Query String")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  /*Por defecto el body no está habilitado para el GET y para habilitarlo hay que importar el body-parser
  que tiene que usar el formato json, sino no lo entiende (el body tiene que estar en formato json)
  si en el postman pongo el body en formato text el resultado de esta parte es vacío {} */
  console.log("Body")
  console.log(req.body)

  res.send("Terminado monstruo")
})

app.post('/apitechu/v2/usuarios', function(req, res){
  //definimos variable nuevo que recoge los valores desde el body
  //var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
  var nuevo = req.body
  //añadimos a la colección
  usuarios.push(nuevo)
  //los objetos json son java script y hay que convertirlos en string, para eso se usa la funcion stringify
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios.json", datos, "utf8", function(err){
    if (err)
      console.log(err)
    console.log("Fichero guardado")
    })
  res.send("Alta OK /apitechu/v2/usuarios")
})

//práctica creación de login
app.post('/apitechu/v2/usuarios/login', function(req, res){
  var email = req.headers.email
  var password = req.headers.password
  var idusuario = -1

  //buscar en la colección si lo encuentra retorna el id, caso contrario retorna mensaje de no coincidencia
  for (var i = 0; i < listausuarios.length; i++) {
    if (listausuarios[i].email == email && listausuarios[i].password == password)
    {
        //respuesta = listausuarios[i].id
        idusuario = listausuarios[i].id
        listausuarios[i].logged = true
        break;
    }
  }
  /*
  console.log("identificador usuario: "+respuesta)
  res.send("Identificador de usuario: " + respuesta)
  */
  if(idusuario!=0)
    res.send({"encontrado":"si", "id":idusuario})
  else {
    res.send({"encontrado":"no"})
  }
})

//practica creación de logout
app.post('/apitechu/v2/usuarios/logout', function(req, res){
  var id = req.headers.id
  var encontrado  = false

  for (var i = 0; i < listausuarios.length; i++) {
    if (listausuarios[i].id == id && listausuarios[i].logged == true)
    {
      listausuarios[i].logged = false
      encontrado =  true;
      break;
    }
  }

  if(encontrado){
    res.send("Usuario "+ listausuarios[i].id +" deslogueado correctamente.")
    console.log("Usuario "+ listausuarios[i].id +" deslogueado correctamente.")
  }
  else {
      res.send("Usuario no se ha logueado previamente.")
      console.log("Usuario no se ha logueado previamente.")
  }
})
