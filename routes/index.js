var express = require('express');
var router = express.Router();

//var auth = require('../auth/auth');
var usuario = require('../dao/usuarios');
//var cuentas = require('../dao/cuentas');

/*
 * Login, accesible por cualquiera, la lógica en auth/validate.js
 */
/**router.post('/login', auth.login);
router.post('/logout', auth.logout);

/*
 * Routes para el usuario
 */
router.get('/usuarios', usuario.getAll);
router.get('/usuarios/:id', usuario.getOne);
router.post('/usuarios/', usuario.create);
router.put('/usuarios/:id', usuario.update);

/*
* Routes para las cuentas, los movimientos son un array en las cuentas

router.get('/cuentas', cuentas.getAll);
router.get('/cuentas/:id', cuentas.getOne);
router.post('/cuentas/', cuentas.create);
router.get('/cuentas/movimientos/:id', cuentas.getAllMovimientos)
router.post('/cuentas/movimientos/', cuentas.addMovimiento)
*/
module.exports = router;
