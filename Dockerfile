# Imagen raíz
FROM node
#Carpeta raíz
WORKDIR /apitechu
#copia de archivos
ADD . /apitechu
#instalo los paquetes necesarios
RUN npm install
#Volumen de la imagen
VOLUME ["/logs"]
#puerto a exponer
EXPOSE 3000
# Comando de inicio, cuando ejecuto el contenedor qué comandos lo ejecutan
CMD ["npm", "start"]
