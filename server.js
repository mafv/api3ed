// BASE SETUP
// =============================================================================
// call the packages we need

//queremos tener una variable para representar a la biblioteca express
var express     = require('express')
//importa el módulo de body-parser
var bodyParser  = require('body-parser')
//importar para habilitar todo los CORS CORS Requests
var cors        = require('cors')
//importar libreria de encriptación que permite generar el hash de cualquier campo
var bcrypt      = require('bcrypt');
var config      = require('./config/config');
var usuario     = require('./dao/usuarios');
var morgan     = require('morgan');

// crea una instancia del servidor express
var app =  express()
app.use(cors())
// configure app
app.use(morgan('dev')); // log requests to the console

var requestJson = require('request-json')
// configure app to use bodyParser()
// esto nos permitirá obtener los datos desde un método POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//crear cliente de mLab (equivalente a database connections)
var urlmLabRaiz = config.urlmLabRaiz;
//recuperar la key desde mlab en user /api Key
var apiKey = config.apiKey;
//definir cliente para mLab
var clienteMLab = null

app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept');
  // If someone calls with method OPTIONS, let's display the allowed methods on our API
  if (req.method == 'OPTIONS') {
    res.status(200);
    res.write("Allow: GET,PUT,POST,DELETE,OPTIONS");
    res.end();
  } else {
    next();
  }
});


/// BASE DE DATOS
//definición de conexión a base de datos
var mongoose   = require('mongoose');
mongoose.connect(config.cadenaConexionBD); // connect to our database

// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});

//definir esquema de usuario
var Usuario = require('./models/usuario');
var InfoCuenta = require('./models/cuenta');
var cuentas = require('./dao/cuentas');
var auth = require('./auth/auth');

// ROUTES para nuestra API
// =============================================================================
var router = express.Router();              // instancia express Router

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'Hola! Bienvenido a mi apitechu' });
});

// on routes that end in /bears
// ----------------------------------------------------
router.route('/usuarios')
// create a usuario (accessed at POST http://localhost:8080/apitechu/usuario)
.post(function(req, res) {
  auth.encryptPass(req.body.password, function(hash) {
      console.log("Encriptamos password:" + req.body.password)
      console.log("password hash:"+ hash)
      //passwordhash = hash;
      var usuario = new Usuario({		// create a new instance of the Usuario model
        id:req.body.idcliente,  // set the usuario name (comes from the request)
        nombre:req.body.nombre,
        apellido:req.body.apellido,
        password:hash,
        email:req.body.email
      });
      usuario.save(function(err) {
          if (err){
            console.log(res.statusCode)
            console.log(err)
            res.send(err)
          }
          else{
                console.log(usuario)
                res.json({ message: 'Usuario creado con exito!' , usuario:usuario})
              }
          })
  });
})

// recupera la lista de TODOS los usuarios (accessed at GET http://localhost:8080/apitechu/usuarios)
	.get(function(req, res) {
    console.log("entramos en el get de usuarios")
		Usuario.find(function(err, usuario) {
			if (err){
          console.log(res.statusCode)
			    res.send(err);
      }else{
        console.log(usuario)
			     res.json(usuario)
      }
		})
	});

  // on routes that end in /usuarios/:_id
  // ----------------------------------------------------
  router.route('/usuarios/:_id')

  	// get the usuario with that id
  	.get(function(req, res) {
  		Usuario.findById(req.params._id, function(err, usuario) {
  			if (err){
          console.log("error:" + err)
  				res.send(err)
        }
        else{
          console.log(req.params)
          console.log("usuario consultado por id:" + req.params._id)
  			  res.json(usuario)
      }
  		})
  	})

//definición de router para tratamiento de cuentas
router.get( '/cuentas', cuentas.getAllCuentas)
router.post('/cuentas', cuentas.createCuenta)
//router.get( '/cuentas/iban/:IBAN', cuentas.getInfoCuenta)
router.get( '/cuentas/cliente', cuentas.getCuentasCliente)
router.post('/cuentas/movimientos/:IBAN', cuentas.addMovimientos)
router.get('/cuentas/movimientos', cuentas.getMovimientosCuenta)

router.post('/login', auth.login);
/**
 * console.log("estas son las router que tenemos registradas:");
 * console.log(router);
 */

// REGISTER OUR ROUTES -------------------------------
app.use('/apitechu', router);

//app.use('/',require('./routes'));

//definimos nuestro puerto de escucha
var port = process.env.PORT || 3000
// Iniciamos el SERVIDOR
// =============================================================================
app.listen(port)
console.log("API escuchando en el puerto y con CORS-enabled " + port)
